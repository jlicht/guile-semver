;;; guile-semver --- Semantic Version tooling for guile
;;; Copyright © 2017 Jelle Dirk Licht <jlicht@fsfe.org>
;;;
;;; This file is part of guile-semver.
;;;
;;; guile-semver is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-semver is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-semver.  If not, see <http://www.gnu.org/licenses/>.

(define-module (semver)
  #:use-module (semver parser)
  #:use-module (semver parser-range)
  #:use-module (semver matcher)
  #:use-module (semver comparator)
  #:export (parse-semver
            valid?
	    satisfies
	    gt
	    lt))

(define (parse-semver str)
  (let ((ast (parse-string str)))
    (ast->semantic-version ast)))

(define (valid? version-str)
  (parse-semver version-str))

(define (satisfies version-str range-str)
  (let ((range (parse-semver-range range-str))
	(version (parse-semver version-str)))
    ((semver-range-matcher range) version)))

(define (gt version-str1 version-str2)
  (= 1 (semantic-version-compare (parse-semver version-str1)
                                 (parse-semver version-str2))))

(define (lt version-str1 version-str2)
  (= -1 (semantic-version-compare (parse-semver version-str1)
                                  (parse-semver version-str2))))
