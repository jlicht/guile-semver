\input texinfo
@c -*-texinfo-*-

@c %**start of header
@setfilename guile-semver.info
@documentencoding UTF-8
@include version.texi
@settitle guile-semver @value{VERSION}
@c %**end of header

@copying
This manual is for guile-semver, version @value{VERSION}.

Copyright @copyright{} 2018 Jelle Licht
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".
@end copying

@titlepage
@title guile-semver Reference Manual

@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

@contents


@ifnottex
@node Top
@top guile-semver

This manual is for guile-semver version @value{VERSION}.
@end ifnottex

@menu
* Introduction::     Why guile-semver.
* Installation::     Installing guile-semver.
@end menu

@node Introduction
@chapter Introduction

Guile-semver is a GNU Guile library for working with version constraint as
defined by the @url{https://semver.org/, Semantic Versioning 2.0} scheme.

One of the goals of guile-semver is to offer a stable toolkit for emulating
version-resolving schemes use by package managers such as
@url{https://www.npmjs.com/, Node Package Manager}. Guile-semver is still in
early stages of development but might already prove usable. None of the
internal modules have a stable API.

This documentation is a stub.

@node Installation
@chapter Installation

@menu
* Requirements::      Software needed to build and run guile-semver.
@end menu

@node Requirements
@section Requirements

Guile-semver depends on the following packages:

@itemize
@item @url{https://www.gnu.org/software/guile/, GNU Guile}
@item @url{http://www.gnu.org/software/make/, GNU Make}.
@end itemize

Current instructions exists in the guix.scm file.

@bye
