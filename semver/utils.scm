;;; guile-semver --- Semantic Version tooling for guile
;;; Copyright © 2017 Jelle Dirk Licht <jlicht@fsfe.org>
;;;
;;; This file is part of guile-semver.
;;;
;;; guile-semver is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; guile-semver is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-semver.  If not, see <http://www.gnu.org/licenses/>.

(define-module (semver utils)
  #:export (juxt))

(define (juxt . fns)
  "Returns the juxtaposition of FNS. The returned function returns a
list containing the result of applying each function in FNS in order."
  (lambda (. v)
    (let loop ((acc '())
	       (fns fns))
      (if (null? fns)
	  (reverse acc)
	  (loop (cons (apply (car fns) v) acc)
		(cdr fns))))))

