;; -*- geiser-scheme-implementation: guile -*-
;;; guix.scm -- Guix package definition

;;; guile-semver --- Semantic Versioning tooling for guile
;;; Copyright © 2017 Jelle Dirk Licht <jlicht@fsfe.org>

;;; Also borrowing code from:
;;; guile-sdl2 --- FFI bindings for SDL2
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;; Mes --- Maxwell Equations of Software
;;; Copyright © 2016,2017,2018 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>

;;;
;;; guix.scm: This file is part of guile-semver.
;;;
;;; guile-semver is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by the Free
;;; Software Foundation; either version 3 of the License, or (at your option)
;;; any later version.
;;;
;;; guile-semver is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;;; for more details.
;;;
;;; You should have received a copy of the GNU General Public License along
;;; with guile-semver.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; GNU Guix development package.  To build and install, run:
;;
;;   guix package -f guix.scm
;;
;; To build it, but not install it, run:
;;
;;   guix build -f guix.scm
;;
;; To use as the basis for a development environment, run:
;;
;;   guix environment -l guix.scm
;;
;;; Code:

(use-modules (srfi srfi-1)
             (srfi srfi-26)
             (ice-9 match)
             (ice-9 popen)
             (ice-9 rdelim)
             (guix build utils)
             (guix build-system gnu)
             (guix download)
             (guix gexp)
             (guix git-download)
             (guix licenses)
             (guix packages)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages base)
             (gnu packages code)
             (gnu packages guile)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define %source-dir (dirname (current-filename)))

;; TODO: create utility function using guile-git
(define git-file?
  (let* ((pipe (with-directory-excursion %source-dir
                 (open-pipe* OPEN_READ "git" "ls-files")))
         (files (let loop ((lines '()))
                  (match (read-line pipe)
                    ((? eof-object?)
                     (reverse lines))
                    (line
                     (loop (cons line lines))))))
         (status (close-pipe pipe)))
    (lambda (file stat)
      (match (stat:type stat)
        ('directory #t)
        ((or 'regular 'symlink)
         (any (cut string-suffix? <> file) files))
        (_ #f)))))

(define-public guile-semver
  (let ((commit "aacff7326f27830ac0f3bad064888909a4c170b8")
        (revision "0")
        (version "0.1.0"))
    (package
      (name "guile-semver")
      (version (string-append version "-" revision "." (string-take commit 7)))
      (source (local-file %source-dir
                          #:recursive? #t
                          #:select? (git-predicate %source-dir)))
      (source #f)
      (build-system gnu-build-system)
      (arguments
       '(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'bootstrap
             (lambda _ (invoke "sh" "bootstrap.sh"))))))
      (native-inputs
       `(("pkg-config" ,pkg-config)
         ("autoconf" ,autoconf)
         ("lcov" ,lcov)		   ; For generating test coverage data
         ("automake" ,automake)
         ("texinfo" ,texinfo)))
      (inputs
       `(("guile" ,guile-2.2)))
      (home-page "https://git.fsfe.org/jlicht/guile-semver")
      (synopsis "Semantic Versioning tooling for Guile")
      (description "guile-semver provides similar tooling to the node semver
package for reading and manipulating datums coforming to the Semantic versioning
standard.")
      (license gpl3+))))

(define-public guile-semver.git
 (let ((version "0.1.0")
       (revision "0")
       (commit (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ))))
    (package
      (inherit guile-semver)
      (name "guile-semver.git")
      (version (string-append version "-" revision "." (string-take commit 7)))
      (source (local-file %source-dir #:recursive? #t #:select? git-file?)))))

guile-semver.git
